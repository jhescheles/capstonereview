<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="/capstone-review/jsapp/bower_components/bootstrap/dist/css/bootstrap.css">
<title>Pokedex Master</title>
</head>
<body ng-app='mainApp'>
<h1 class="jumbotron">{{"welcome"}}</h1>
<ng-view></ng-view>

<script src="../bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="/capstone-review/jsapp/bower_components/angular/angular.js"></script>
<script type="text/javascript" src="/capstone-review/jsapp/bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="../angular-route/angular-route.min.js"></script>
<script src="../application/app.js"></script>
<script src="../application/controllers/mainCtrl.js"></script>
<script src="../application/controllers/trainerCtrl.js"></script>
<script src="../application/controllers/oakCtrl.js"></script>
</body>
</html>