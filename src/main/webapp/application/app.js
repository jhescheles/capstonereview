var mainApp = angular.module('mainApp', ['ngRoute']);


mainApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/welcome',
			{
				templateUrl: '../application/partials/mainTempl.html',
				controller: 'mainCtrl'
			}
	);
	$routeProvider.when('/trainerview',
			{
				templateUrl: '../application/partials/trainerviewTempl.html',
				controller: 'trainerCtrl'
			}
	);
	$routeProvider.when('/oakview',
			{
				templateUrl: '../application/partials/oakviewTempl.html',
				controller: 'oakCtrl'
			}
	);
	

	
	$routeProvider.otherwise({redirectTo: 'welcome'});
}]);