'use strict';

// Declare app level module which depends on views, and components
var exampleApp = angular.module('exampleApp', ['ngRoute', 'ngResource']);


exampleApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/',
			{
				templateUrl: '/capstone-review/jsapp/example/templates/example.tpl.html',
				controller: 'exampleCtrl',
				resolve: {
					players:function(exPlayerService){
						return exPlayerService.getPlayers();
					}
				}
			}
	);
		
	$routeProvider.otherwise({redirectTo: '/'});
}]);
