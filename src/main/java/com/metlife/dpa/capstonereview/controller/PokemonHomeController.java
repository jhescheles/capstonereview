package com.metlife.dpa.capstonereview.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PokemonHomeController {
	@RequestMapping("/pokemon")
	public String index(){
		return "pokemonIndex";
	}
}
