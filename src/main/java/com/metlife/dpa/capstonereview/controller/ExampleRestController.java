package com.metlife.dpa.capstonereview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.dpa.capstonereview.model.Player;
import com.metlife.dpa.capstonereview.repository.PlayerRepository;

@RestController
public class ExampleRestController {

	@Autowired
	PlayerRepository playerRepository;
	
	@RequestMapping("/players")
	public List<Player> players(){
		return playerRepository.findAll();
	}
	
	@RequestMapping(value="/seedplayers", method=RequestMethod.POST)
	public void seedPlayers(){
		List<Player> players = new ArrayList<Player>();
		players.add(new Player("Eric Staal", "Center"));
		players.add(new Player("Cam Ward", "Goal"));
		players.add(new Player("Jeff Skinner", "Left Wing"));
		
		playerRepository.save(players);
	}
}
