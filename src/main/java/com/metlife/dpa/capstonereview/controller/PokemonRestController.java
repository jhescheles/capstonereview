package com.metlife.dpa.capstonereview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.dpa.capstonereview.model.Pokemon;
import com.metlife.dpa.capstonereview.repository.PokemonRepository;

@RestController
public class PokemonRestController {

	@Autowired
	PokemonRepository pokemonRepository;
	
	@RequestMapping("/pokemonList")
	public List<Pokemon> players(){
		return pokemonRepository.findAll();
	}
	
	@RequestMapping(value="/seedpokemon", method=RequestMethod.POST)
	public void seedPlayers(){
		List<Pokemon> pokemon = new ArrayList<Pokemon>();
		pokemon.add(new Pokemon("Jigglypuff", "noob", "supernoob","meganoob"));
		pokemon.add(new Pokemon("igglypuff", "noob", "supernoob","meganoob"));
		pokemon.add(new Pokemon("WigglyTuff", "noob", "supernoob","meganoob"));
		
		pokemonRepository.save(pokemon);
	}
}
