package com.metlife.dpa.capstonereview.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection="pokemon")
public class Pokemon {
	
	@Id private String id;
	private String name;
	private String type1;
	private String type2;
	private String description;
	
	public String getDescription(){
		return description;
	}
	public void setDescription(String description){
		this.description = description;
	}
	
	public String getType1() {
		return type1;
	}
	public String getType2(){
		return type2;
	}

	public void setType1(String type) {
		this.type1 = type;
	}
	public void setType2(String type) {
		this.type2 = type;
	}

	public Pokemon(){}
	
	public Pokemon(String id,String name, String type1, String type2){
		this.id=id;
		this.name = name;
		this.type1 = type1;
		this.type2= type2;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}